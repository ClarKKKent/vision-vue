<?php


namespace App\UI\Rest\User\Auth;


use App\Application\User\Auth\Command\SigninUser\SigninUserCommand;
use App\Application\User\Auth\Query\GetToken\GetTokenQuery;
use App\Infrastructure\Core\UI\RestCQRSController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


final class SigninAction extends RestCQRSController
{
    /**
     * @Route("/api/v1/auth/signin", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function __invoke(Request $request): JsonResponse
    {
        $this->exec(new SigninUserCommand($request->get('email'), $request->get('password')));

        return $this->json([
            'token' => $this->ask(new GetTokenQuery($request->get('email')))
        ], 200);
    }
}