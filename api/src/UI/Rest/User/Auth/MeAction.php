<?php


namespace App\UI\Rest\User\Auth;


use App\Application\User\Auth\Query\GetMe\GetMeQuery;
use App\Infrastructure\Core\UI\RestCQRSController;
use Symfony\Component\Routing\Annotation\Route;

final class MeAction extends RestCQRSController
{
    /**
     * @Route("/api/v1/auth/me", methods={"GET"})
     */
    public function __invoke()
    {
        $user = $this->ask(new GetMeQuery());

        return $this->ok($user);
    }
}