<?php


namespace App\UI\Rest\Sport\Exercise;



use App\Application\Sport\Exercise\Command\DeleteExercise\DeleteExerciseCommand;
use App\Infrastructure\Core\UI\RestCQRSController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

final class DeleteAction extends RestCQRSController
{
    /**
     * @Route("/api/v1/sport/exercises/{id}", methods={"DELETE"})
     * @param string $id
     * @return JsonResponse
     */
    public function __invoke(string $id): JsonResponse
    {
        $this->exec(
            new DeleteExerciseCommand($id)
        );

        return $this->noContent();
    }
}