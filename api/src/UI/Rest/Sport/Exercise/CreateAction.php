<?php


namespace App\UI\Rest\Sport\Exercise;


use App\Application\Sport\Exercise\Command\CreateExercise\CreateExerciseCommand;
use App\Infrastructure\Core\UI\RestCQRSController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

final class CreateAction extends RestCQRSController
{
    /**
     * @Route("/api/v1/sport/exercises", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function __invoke(Request $request): JsonResponse
    {
        $this->exec(
            new CreateExerciseCommand($request->get('title'), $request->get('description'))
        );

        return $this->okCreated('Exercise successfully created.');
    }
}