<?php


namespace App\UI\Rest\Sport\Result;


use App\Application\Sport\Result\Command\DeleteResult\DeleteResultCommand;
use App\Infrastructure\Core\UI\RestCQRSController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

final class DeleteAction extends RestCQRSController
{
    /**
     * @Route("/api/v1/sport/results/{id}", methods={"DELETE"})
     *
     * @param string $id
     * @param Request $request
     * @return JsonResponse
     */
    public function __invoke(string $id, Request $request): JsonResponse
    {
        $command = new DeleteResultCommand($id);

        $this->exec($command);

        return $this->noContent();
    }
}