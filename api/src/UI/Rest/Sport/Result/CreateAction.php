<?php


namespace App\UI\Rest\Sport\Result;


use App\Application\Sport\Result\Command\CreateResult\CreateResultCommand;
use App\Infrastructure\Core\UI\RestCQRSController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

final class CreateAction extends RestCQRSController
{
    /**
     * @Route("/api/v1/sport/results", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function __invoke(Request $request): JsonResponse
    {
        $command = new CreateResultCommand(
            $request->get('weight'),
            $request->get('quantity'),
            $request->get('exercise_id')
        );

        $this->exec($command);

        return $this->okCreated('Result successfully created.');
    }
}