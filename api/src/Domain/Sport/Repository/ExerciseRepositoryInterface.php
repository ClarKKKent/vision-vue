<?php


namespace App\Domain\Sport\Repository;


use App\Domain\Core\UuidInterface;
use App\Domain\Sport\Entity\Exercise;

interface ExerciseRepositoryInterface
{
    public function nextIdentity(): UuidInterface;

    public function save(Exercise $exercise): void;

    public function delete(Exercise $exercise): void;

    public function oneById(UuidInterface $id): Exercise;
}