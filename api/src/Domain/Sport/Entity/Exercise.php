<?php


namespace App\Domain\Sport\Entity;


use App\Domain\Core\UuidInterface;
use App\Domain\User\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Exercise
{
    private UuidInterface $id;

    private string $title;

    private string $description;

    private ?User $user;

    private Collection $results;

    public function __construct(
        UuidInterface $id,
        string $title,
        string $description)
    {
        $this->id = $id;
        $this->title = $title;
        $this->description = $description;
        $this->user = null;
        $this->results = new ArrayCollection();
    }

    public static function create(
        UuidInterface $id,
        string $title,
        string $description): self
    {
        return new self(
            $id,
            $title,
            $description
        );
    }

    public function update(string $title, string $description, ?User $user)
    {
        $this->title = $title;
        $this->description = $description;
        $this->user = $user;
    }

    public function applyResult(Result $result): self
    {
        if ($this->results->contains($result)) {
            throw new \DomainException('Result is already attached.');
        }
        $this->results[] = $result;
        $result->applyExercise($this);

        return $this;
    }

    public function revokeResult(Result $result): void
    {
        if (!$this->results->contains($result)) {
            throw new \DomainException('Result is not attached');
        }
        $this->results->removeElement($result);
    }

    public function attachUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function getResults(): Collection
    {
        return $this->results;
    }
}