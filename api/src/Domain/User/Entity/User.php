<?php


namespace App\Domain\User\Entity;


use App\Domain\Sport\Entity\Exercise;
use App\Domain\Sport\Entity\Result;
use App\Domain\User\Exception\ExerciseAlreadyExistException;
use App\Domain\User\Exception\UserNotHaveExerciseException;
use App\Domain\User\Specification\UniqueEmailSpecificationInterface;
use App\Domain\User\ValueObject\Email;
use App\Domain\Core\UuidInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;


class User
{
    private UuidInterface $id;

    private Email $email;

    private string $passwordHash;

    private Collection $exercises;

    private Collection $results;

    public function __construct(UuidInterface $id, Email $email, string $passwordHash)
    {
        $this->id = $id;
        $this->email = $email;
        $this->passwordHash = $passwordHash;
        $this->exercises = new ArrayCollection();
        $this->results = new ArrayCollection();
    }

    public static function create(
        UuidInterface $id,
        Email $email,
        string $passwordHash,
        UniqueEmailSpecificationInterface $specification): self
    {
        $specification->isSatisfiedBy($email);

        return new self(
            $id,
            $email,
            $passwordHash
        );
    }

    public function attachExercise(Exercise $exercise): self
    {
        if ($this->exercises->contains($exercise)) {
            throw new \DomainException('Exercise is already attached.');
        }
        $this->exercises[] = $exercise;
        $exercise->attachUser($this);

        return $this;
    }

    public function detachExercise(Exercise $exercise): self
    {
        if (!$this->exercises->contains($exercise)) {
            throw new \DomainException('Exercise is not attached.');
        }
        $this->exercises->removeElement($exercise);

        if ($exercise->getUser() === $this) {
            $exercise->attachUser(null);
        }
        return $this;
    }

    public function applyResult(Result $result): void
    {
        if ($this->results->contains($result)) {
            throw new \DomainException('Result is already applied.');
        }

        $this->results[] = $result;
        $result->attachUser($this);
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getEmail(): Email
    {
        return $this->email;
    }

    public function getPasswordHash(): string
    {
        return $this->passwordHash;
    }
}