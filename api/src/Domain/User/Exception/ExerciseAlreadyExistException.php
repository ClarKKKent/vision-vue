<?php


namespace App\Domain\User\Exception;


use Throwable;

final class ExerciseAlreadyExistException extends \LogicException
{
    public function __construct($message = "Exercise already exist.", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}