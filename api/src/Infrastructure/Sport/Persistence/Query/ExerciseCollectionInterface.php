<?php


namespace App\Infrastructure\Sport\Persistence\Query;


use App\Application\Sport\Exercise\Query\GetAllExercises\GetAllExercisesQuery;
use App\Application\Sport\Exercise\Query\GetExercisesList\GetExercisesListQuery;
use App\Infrastructure\Sport\Persistence\ReadModel\ExerciseView;

interface ExerciseCollectionInterface
{
    public function one(string $id): ExerciseView;

    public function list(GetExercisesListQuery $query): array;

    public function all(GetAllExercisesQuery $query): array;
}