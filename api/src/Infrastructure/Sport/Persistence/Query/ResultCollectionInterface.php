<?php


namespace App\Infrastructure\Sport\Persistence\Query;


use App\Application\Sport\Result\Query\GetResult\GetResultQuery;
use App\Application\Sport\Result\Query\GetResultsList\GetResultsListQuery;
use App\Infrastructure\Sport\Persistence\ReadModel\ResultView;

interface ResultCollectionInterface
{
    public function list(GetResultsListQuery $query): array;

    public function one(GetResultQuery $query): ResultView;
}