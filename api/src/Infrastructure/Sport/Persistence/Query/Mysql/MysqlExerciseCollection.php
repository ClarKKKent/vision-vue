<?php


namespace App\Infrastructure\Sport\Persistence\Query\Mysql;


use App\Application\Sport\Exercise\Query\GetAllExercises\GetAllExercisesQuery;
use App\Application\Sport\Exercise\Query\GetExercisesList\GetExercisesListQuery;
use App\Domain\Sport\Entity\Exercise;
use App\Domain\User\Entity\User;
use App\Infrastructure\Core\Persistence\Mysql\Query\AbstractMysqlCollection;
use App\Infrastructure\Sport\Persistence\Query\ExerciseCollectionInterface;
use App\Infrastructure\Sport\Persistence\ReadModel\ExerciseView;

final class MysqlExerciseCollection extends AbstractMysqlCollection implements ExerciseCollectionInterface
{
    public function one(string $id): ExerciseView
    {
        $qb = $this->entityManager->getConnection()->createQueryBuilder()
            ->select('e.*')
            ->from($this->tableName(Exercise::class), 'e')
            ->leftJoin('e', $this->tableName(User::class), 'u', 'e.user_id = u.id')
            ->where('e.id = :exercise')
            ->setParameter(':exercise', $id);

        $query = $this->execute($qb);
        $query->setFetchMode(\PDO::FETCH_CLASS, ExerciseView::class);

        return $query->fetch();
    }

    public function list(GetExercisesListQuery $query): array
    {
        $qb = $this->entityManager->getConnection()->createQueryBuilder()
            ->select('e.*')
            ->from($this->tableName(Exercise::class), 'e')
            ->leftJoin('e', $this->tableName(User::class), 'u', 'e.user_id = u.id')
            ->where('u.id = :user')
            ->setParameter(':user', $query->userId);

        $query = $this->execute($qb);
        $query->setFetchMode(\PDO::FETCH_CLASS, ExerciseView::class);

        return $query->fetchAll();
    }

    public function all(GetAllExercisesQuery $query): array
    {
        $qb = $this->entityManager->getConnection()->createQueryBuilder()
            ->select('e.*')
            ->from($this->tableName(Exercise::class), 'e')
            ->leftJoin('e', $this->tableName(User::class), 'u', 'e.user_id = u.id')
            ->where('u.id = :user')
            ->setParameter(':user', $query->userId);

        $query = $this->execute($qb);
        $query->setFetchMode(\PDO::FETCH_CLASS, ExerciseView::class);

        return $query->fetchAll();
    }
}