<?php


namespace App\Infrastructure\Sport\Persistence\Query\Mysql;


use App\Application\Sport\Result\Query\GetResult\GetResultQuery;
use App\Application\Sport\Result\Query\GetResultsList\GetResultsListQuery;
use App\Domain\Sport\Entity\Exercise;
use App\Domain\Sport\Entity\Result;
use App\Infrastructure\Core\Persistence\Mysql\Query\AbstractMysqlCollection;
use App\Infrastructure\Sport\Persistence\Query\ResultCollectionInterface;
use App\Infrastructure\Sport\Persistence\ReadModel\ResultView;


final class MysqlResultCollection extends AbstractMysqlCollection implements ResultCollectionInterface
{
    public function list(GetResultsListQuery $query): array
    {
        $qb = $this->entityManager->getConnection()->createQueryBuilder()
            ->select('r.*')
            ->from($this->tableName(Result::class), 'r')
            ->leftJoin(
                'r', $this->tableName(Exercise::class), 'e', 'r.exercise_id = e.id')
            ->where('r.user_id = :user')
            ->setParameter(':user', $query->userId);

        $query = $this->execute($qb);
        $query->setFetchMode(\PDO::FETCH_CLASS, ResultView::class);

        return $query->fetchAll();
    }

    public function one(GetResultQuery $query): ResultView
    {
        $qb = $this->entityManager->getConnection()->createQueryBuilder()
            ->select('r.*')
            ->from($this->tableName(Result::class), 'r')
            ->leftJoin(
                'r', $this->tableName(Exercise::class), 'e', 'r.exercise_id = e.id')
            ->where('r.id = :id')
            ->setParameter(':id', $query->id);

        $query = $this->execute($qb);
        $query->setFetchMode(\PDO::FETCH_CLASS, ResultView::class);

        return $query->fetch();
    }
}