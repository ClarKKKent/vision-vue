<?php


namespace App\Infrastructure\Sport\Persistence\Store\Mysql;

use App\Domain\Core\UuidInterface;
use App\Domain\Sport\Entity\Exercise;
use App\Domain\Sport\Repository\ExerciseRepositoryInterface;
use App\Infrastructure\Core\Exception\ModelNotFoundException;
use App\Infrastructure\Core\Persistence\Doctrine\Repository\AbstractMysqlRepository;

final class MysqlExerciseRepository extends AbstractMysqlRepository implements ExerciseRepositoryInterface
{
    protected function getClass(): string
    {
        return Exercise::class;
    }

    public function save(Exercise $exercise): void
    {
        $this->entityManager->persist($exercise);
        $this->entityManager->flush();
    }

    public function delete(Exercise $exercise): void
    {
        $this->entityManager->remove($exercise);
        $this->entityManager->flush();
    }

    public function oneById(UuidInterface $id): Exercise
    {
        /** @var $exercise Exercise */
        if (!$exercise = $this->repository->find($id->toString())) {
            throw new ModelNotFoundException('Exercise not found.');
        }
        return $exercise;
    }
}