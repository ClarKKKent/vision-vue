<?php


namespace App\Infrastructure\Sport\Persistence\ReadModel;


use App\Infrastructure\Core\Persistence\ReadModelInterface;

final class ResultView implements ReadModelInterface
{
    public ?string $id;

    public ?string $weight;

    public ?string $quantity;

    public ?string $exercise_id;

    public ?string $title = null;

    public ?string $description = null;

    public function serialize(): array
    {
        return [
            'id' => $this->id,
            'weight' => $this->weight,
            'quantity' => $this->quantity,
            'exercise' => [
                'id' => $this->exercise_id,
                'title' => $this->title,
                'description' => $this->description
            ]
        ];
    }
}