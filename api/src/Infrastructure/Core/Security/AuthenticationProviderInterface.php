<?php


namespace App\Infrastructure\Core\Security;


use App\Infrastructure\User\Persistence\ReadModel\AuthView;

interface AuthenticationProviderInterface
{
    public function generateToken(AuthView $user): string;
}