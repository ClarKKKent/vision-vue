<?php


namespace App\Infrastructure\Core\Security\Password\Bcrypt;


use App\Infrastructure\Core\Security\Password\PasswordHasherInterface;

final class BcryptPasswordHasher implements PasswordHasherInterface
{
    public function hash(string $password): string
    {
        return password_hash($password, PASSWORD_BCRYPT);
    }

    public function verify(string $password, string $passwordHash): bool
    {
        return password_verify($password, $passwordHash);
    }
}