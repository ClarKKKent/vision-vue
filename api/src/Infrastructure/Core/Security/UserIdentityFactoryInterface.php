<?php


namespace App\Infrastructure\Core\Security;


use App\Infrastructure\User\Persistence\ReadModel\AuthView;
use Symfony\Component\Security\Core\User\UserInterface;

interface UserIdentityFactoryInterface
{
    public function createFromUser(AuthView $user): UserInterface;
}