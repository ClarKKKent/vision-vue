<?php


namespace App\Infrastructure\Core\Security\JWT;


use App\Infrastructure\Core\Security\AuthenticationProviderInterface;
use App\Infrastructure\Core\Security\UserIdentityFactoryInterface;
use App\Infrastructure\User\Persistence\ReadModel\AuthView;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

final class AuthenticationProvider implements AuthenticationProviderInterface
{
    private JWTTokenManagerInterface $jwtManager;

    private UserIdentityFactoryInterface $identityFactory;

    public function __construct(JWTTokenManagerInterface $jwtManager, UserIdentityFactoryInterface $identityFactory)
    {
        $this->jwtManager = $jwtManager;
        $this->identityFactory = $identityFactory;
    }

    public function generateToken(AuthView $user): string
    {
        $userIdentity = $this->identityFactory->createFromUser($user);

        return $this->jwtManager->create($userIdentity);
    }
}