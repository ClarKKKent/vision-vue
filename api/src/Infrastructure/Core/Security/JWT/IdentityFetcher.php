<?php


namespace App\Infrastructure\Core\Security\JWT;


use App\Infrastructure\Core\Exception\AuthenticationException;
use App\Infrastructure\Core\Security\IdentityFetcherInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;

final class IdentityFetcher implements IdentityFetcherInterface
{
    private TokenStorageInterface $tokenStorage;

    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function get(): UserInterface
    {
        $token = $this->tokenStorage->getToken();

        if (!$token) {
            throw new AuthenticationException();
        }

        $user = $token->getUser();

        if (!$user instanceof UserIdentity) {
            throw new AuthenticationException();
        }

        return $user;
    }
}