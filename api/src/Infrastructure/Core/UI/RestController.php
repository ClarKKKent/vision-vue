<?php


namespace App\Infrastructure\Core\UI;


use App\Application\Core\Collection;
use App\Application\Core\Item;
use App\Application\Core\PaginatedCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

class RestController extends AbstractController
{
    private JsonApiFormatter $formatter;

    public function __construct(JsonApiFormatter $formatter)
    {
        $this->formatter = $formatter;
    }

    public function ok(Item $resource): JsonResponse
    {
        return $this->json($this->formatter->one($resource), 200);
    }

    public function okCollection(Collection $collection): JsonResponse
    {
        return $this->json($this->formatter->collection($collection), 200);
    }

    public function okPaginatedCollection(PaginatedCollection $collection): JsonResponse
    {
        return $this->json($this->formatter->paginatedCollection($collection), 200);
    }

    public function okCreated(string $message): JsonResponse
    {
        return $this->json([
            'message' => $message
        ], 201);
    }

    public function noContent(): JsonResponse
    {
        return $this->json([], 204);
    }
}