<?php


namespace App\Infrastructure\Core\Behavior;


use App\Application\Core\CommandInterface;
use App\Infrastructure\Core\Exception\ValidationException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Validator\Validator\ValidatorInterface;

final class RequestCommandResolver
{
    private ValidatorInterface $validator;

    private ObjectNormalizer $normalizer;

    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
        $this->normalizer = new ObjectNormalizer(null, new CamelCaseToSnakeCaseNameConverter());
    }

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        $class = $argument->getType();
        $reflection = new \ReflectionClass($class);

        return $reflection->implementsInterface(CommandInterface::class);
    }

    public function resolve(Request $request, ArgumentMetadata $argument)
    {
        /** @var CommandInterface $command */
        $command = $this->normalizer->denormalize($request->request->all(), $argument->getType());
        $this->validateCommand($command);

        yield $command;
    }

    private function validateCommand(CommandInterface $command)
    {
        $errors = $this->validator->validate($command);

        if (count($errors) > 0) {
            throw new ValidationException($errors);
        }
    }
}