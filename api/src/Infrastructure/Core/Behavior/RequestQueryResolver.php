<?php


namespace App\Infrastructure\Core\Behavior;


use App\Application\Core\QueryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

final class RequestQueryResolver
{
    private ObjectNormalizer $normalizer;

    public function __construct()
    {
        $this->normalizer = new ObjectNormalizer(null, new CamelCaseToSnakeCaseNameConverter());
    }

    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        $class = $argument->getType();
        $reflection = new \ReflectionClass($class);

        return $reflection->implementsInterface(QueryInterface::class);
    }

    public function resolve(Request $request, ArgumentMetadata $argument)
    {
        /** @var QueryInterface $query */
        $query = $this->normalizer->denormalize($request->query->all(), $argument->getType());

        yield $query;
    }
}