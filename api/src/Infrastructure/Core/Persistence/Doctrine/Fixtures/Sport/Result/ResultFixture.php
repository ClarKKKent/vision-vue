<?php


namespace App\Infrastructure\Core\Persistence\Doctrine\Fixtures\Sport\Result;


use App\Infrastructure\Core\Persistence\Doctrine\Builders\Sport\Result\ResultBuilder;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ResultFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $result = ResultBuilder::any();

        $manager->persist($result);
        $manager->flush();
    }
}