<?php


namespace App\Infrastructure\Core\Persistence\Doctrine\Fixtures\Sport\Result;


use App\Domain\Core\Uuid;
use App\Domain\Sport\Entity\Result;
use App\Infrastructure\Core\Persistence\Doctrine\Builders\User\User\UserBuilder;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ResultsFixture extends Fixture
{
    public const RESULTS_LENGTH = 3;

    public function load(ObjectManager $manager)
    {
        $user = UserBuilder::any();

        for ($i = 1; $i <= self::RESULTS_LENGTH; $i++) {
            $result = Result::create(
                Uuid::fromString(\Ramsey\Uuid\Uuid::uuid4()),
                15.0,
                5
            );
            $user->applyResult($result);
        }

        $manager->persist($user);
        $manager->flush();
    }
}