<?php


namespace App\Infrastructure\Core\Persistence\Doctrine\Fixtures\Sport\Exercise;


use App\Domain\Core\Uuid;
use App\Domain\Sport\Entity\Exercise;
use App\Infrastructure\Core\Persistence\Doctrine\Builders\Sport\Exercise\ExerciseBuilder;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ExerciseFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $exercise = ExerciseBuilder::any();

        $manager->persist($exercise);
        $manager->flush();
    }
}