<?php


namespace App\Infrastructure\Core\Persistence\Doctrine\Builders\User\User;


use App\Domain\Core\Uuid;
use App\Domain\User\Entity\User;
use App\Domain\User\ValueObject\Email;
use App\Infrastructure\Core\Security\Password\Bcrypt\BcryptPasswordHasher;

class UserBuilder
{
    public const USER_TEST_ID = 'fb3c19ff-584d-419c-a72e-a314b0056b65';

    public static function any(): User
    {
        return new User(
            Uuid::fromString(self::USER_TEST_ID),
            Email::fromString('test@test.com'),
            (new BcryptPasswordHasher())->hash('password')
        );
    }
}