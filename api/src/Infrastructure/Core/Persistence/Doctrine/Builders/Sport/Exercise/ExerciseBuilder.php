<?php


namespace App\Infrastructure\Core\Persistence\Doctrine\Builders\Sport\Exercise;


use App\Domain\Core\Uuid;
use App\Domain\Sport\Entity\Exercise;
use App\Domain\User\Entity\User;
use App\Infrastructure\Core\Persistence\Doctrine\Builders\User\User\UserBuilder;

class ExerciseBuilder
{
    public const EXERCISE_TEST_ID = 'fb3c19ff-584d-419c-a72e-a314b0056b65';

    public static function any(): Exercise
    {
        return new Exercise(
            Uuid::fromString(self::EXERCISE_TEST_ID),
            'test',
            'test'
        );
    }
}