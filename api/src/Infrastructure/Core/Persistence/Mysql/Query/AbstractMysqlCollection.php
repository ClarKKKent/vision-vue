<?php


namespace App\Infrastructure\Core\Persistence\Mysql\Query;


use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\ORM\EntityManagerInterface;

abstract class AbstractMysqlCollection
{
    protected EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    protected function execute(QueryBuilder $qb)
    {
        return $this->entityManager->getConnection()->executeQuery($qb->getSQL(), $qb->getParameters());
    }

    protected function tableName(string $className): string
    {
        return $this->entityManager->getClassMetadata($className)->getTableName();
    }
}