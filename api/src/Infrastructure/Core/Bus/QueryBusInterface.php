<?php


namespace App\Infrastructure\Core\Bus;


use App\Application\Core\QueryInterface;

interface QueryBusInterface
{
    public function handle(QueryInterface $query);
}