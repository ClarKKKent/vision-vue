<?php


namespace App\Infrastructure\User\Persistence\Query;


use App\Infrastructure\User\Persistence\ReadModel\AuthView;
use App\Infrastructure\User\Persistence\ReadModel\UserView;
use Symfony\Component\Security\Core\User\UserInterface;

interface UserCollectionInterface
{
    public function oneByEmailForAuth(string $email): AuthView;

    public function oneByIdentity(UserInterface $identity): UserView;
}