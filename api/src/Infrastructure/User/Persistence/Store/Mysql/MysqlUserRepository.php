<?php


namespace App\Infrastructure\User\Persistence\Store\Mysql;


use App\Domain\User\Entity\User;
use App\Domain\User\Repository\UserRepositoryInterface;
use App\Domain\User\ValueObject\Email;
use App\Infrastructure\Core\Exception\ModelNotFoundException;
use App\Infrastructure\Core\Persistence\Doctrine\Repository\AbstractMysqlRepository;
use Symfony\Component\Security\Core\User\UserInterface;

final class MysqlUserRepository extends AbstractMysqlRepository implements UserRepositoryInterface
{
    protected function getClass(): string
    {
        return User::class;
    }

    public function save(User $user): void
    {
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

    public function delete(User $user): void
    {
        $this->entityManager->remove($user);
        $this->entityManager->flush();
    }

    public function existByEmail(Email $email): bool
    {
        return $this->entityManager->createQueryBuilder()
            ->select('COUNT(u.id)')
            ->from(User::class, 'u')
            ->where('u.email = :email')
            ->setParameter(':email', $email->toString())
            ->getQuery()->getSingleScalarResult()>0;
    }

    public function oneByIdentity(UserInterface $identity): User
    {
        $user = $this->entityManager->createQueryBuilder()
            ->select('u')
            ->from(User::class, 'u')
            ->where('u.email = :email')
            ->setParameter(':email', $identity->getUsername())
            ->getQuery()->getSingleResult();


        if (!$user) {
            throw new ModelNotFoundException('User not found.');
        }

        return $user;
    }
}