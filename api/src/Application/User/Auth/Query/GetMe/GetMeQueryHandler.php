<?php

namespace App\Application\User\Auth\Query\GetMe;

use App\Application\Core\Item;
use App\Application\Core\QueryHandlerInterface;
use App\Infrastructure\Core\Security\IdentityFetcherInterface;
use App\Infrastructure\User\Persistence\Query\UserCollectionInterface;


final class GetMeQueryHandler implements QueryHandlerInterface
{
    private IdentityFetcherInterface $identityFetcher;

    private UserCollectionInterface $userCollection;

    public function __construct(IdentityFetcherInterface $identityFetcher, UserCollectionInterface $userCollection)
    {
        $this->identityFetcher = $identityFetcher;
        $this->userCollection = $userCollection;
    }

    public function __invoke(GetMeQuery $query): Item
    {
        $identity = $this->identityFetcher->get();

        return new Item($this->userCollection->oneByIdentity($identity));
    }
}