<?php

namespace App\Application\User\Auth\Command\CreateSuperUser;


use App\Application\Core\CommandHandlerInterface;

use App\Domain\User\Entity\User;
use App\Domain\User\Repository\UserRepositoryInterface;
use App\Domain\User\ValueObject\Email;
use App\Infrastructure\Core\Security\Password\PasswordHasherInterface;
use App\Infrastructure\User\Specification\UniqueEmailSpecification;


final class CreateSuperUserCommandHandler implements CommandHandlerInterface
{
    private UserRepositoryInterface $userRepository;

    private PasswordHasherInterface $passwordHasher;

    public function __construct(
        UserRepositoryInterface $userRepository,
        PasswordHasherInterface $passwordHasher)
    {
        $this->userRepository = $userRepository;
        $this->passwordHasher = $passwordHasher;
    }

    public function __invoke(CreateSuperUserCommand $message): void
    {
        $user = User::create(
            $this->userRepository->nextIdentity(),
            Email::fromString($message->email),
            $this->passwordHasher->hash($message->password),
            new UniqueEmailSpecification($this->userRepository)
        );

        $this->userRepository->save($user);
    }
}