<?php


namespace App\Application\User\Auth\Command\SigninUser;


use App\Application\Core\CommandInterface;
use Symfony\Component\Validator\Constraints as Assert;

final class SigninUserCommand implements CommandInterface
{
    /**
     * @Assert\NotBlank
     */
    public ?string $email;

    /**
     * @Assert\NotBlank
     */
    public ?string $password;

    public function __construct(?string $email, ?string $password)
    {
        $this->email = $email;
        $this->password = $password;
    }
}