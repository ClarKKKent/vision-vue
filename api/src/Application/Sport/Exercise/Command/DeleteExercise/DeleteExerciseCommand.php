<?php


namespace App\Application\Sport\Exercise\Command\DeleteExercise;


use App\Application\Core\CommandInterface;

final class DeleteExerciseCommand implements CommandInterface
{
    public ?string $id;

    public function __construct(?string $id)
    {
        $this->id = $id;
    }
}