<?php


namespace App\Application\Sport\Exercise\Command\DeleteExercise;


use App\Application\Core\CommandHandlerInterface;
use App\Domain\Core\Uuid;
use App\Domain\Sport\Repository\ExerciseRepositoryInterface;

final class DeleteExerciseCommandHandler implements CommandHandlerInterface
{
    private ExerciseRepositoryInterface $exerciseRepository;

    public function __construct(ExerciseRepositoryInterface $exerciseRepository)
    {
        $this->exerciseRepository = $exerciseRepository;
    }

    public function __invoke(DeleteExerciseCommand $message): void
    {
        $exercise = $this->exerciseRepository->oneById(Uuid::fromString($message->id));

        $this->exerciseRepository->delete($exercise);
    }
}