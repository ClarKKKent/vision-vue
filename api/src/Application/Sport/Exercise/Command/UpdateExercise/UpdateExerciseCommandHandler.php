<?php


namespace App\Application\Sport\Exercise\Command\UpdateExercise;


use App\Application\Core\CommandHandlerInterface;
use App\Domain\Core\Uuid;
use App\Domain\Sport\Entity\Exercise;
use App\Domain\Sport\Repository\ExerciseRepositoryInterface;
use App\Domain\User\Repository\UserRepositoryInterface;
use App\Infrastructure\Core\Security\IdentityFetcherInterface;

final class UpdateExerciseCommandHandler implements CommandHandlerInterface
{
    private ExerciseRepositoryInterface $exerciseRepository;

    private IdentityFetcherInterface $identityFetcher;

    private UserRepositoryInterface $userRepository;

    public function __construct(
        ExerciseRepositoryInterface $exerciseRepository,
        IdentityFetcherInterface $identityFetcher,
        UserRepositoryInterface $userRepository)
    {
        $this->exerciseRepository = $exerciseRepository;
        $this->identityFetcher = $identityFetcher;
        $this->userRepository = $userRepository;
    }

    public function __invoke(UpdateExerciseCommand $message): void
    {
        $user = $this->userRepository->oneByIdentity($this->identityFetcher->get());

        $exercise = $this->exerciseRepository->oneById(Uuid::fromString($message->exerciseId));

        $exercise->update(
            $message->title,
            $message->description,
            $user
        );

        $this->exerciseRepository->save($exercise);
    }
}