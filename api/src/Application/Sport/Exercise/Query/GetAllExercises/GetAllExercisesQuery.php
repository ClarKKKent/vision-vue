<?php


namespace App\Application\Sport\Exercise\Query\GetAllExercises;


use App\Application\Core\QueryInterface;

final class GetAllExercisesQuery implements QueryInterface
{
    public ?string $userId;
}