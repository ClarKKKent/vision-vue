<?php


namespace App\Application\Sport\Exercise\Query\GetExercisesList;


use App\Application\Core\AbstractListQuery;
use App\Application\Core\QueryInterface;

final class GetExercisesListQuery extends AbstractListQuery implements QueryInterface
{
    public ?string $userId = null;
}