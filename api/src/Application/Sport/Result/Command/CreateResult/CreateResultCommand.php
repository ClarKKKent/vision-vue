<?php


namespace App\Application\Sport\Result\Command\CreateResult;


use App\Application\Core\CommandInterface;
use Symfony\Component\Validator\Constraints as Assert;

final class CreateResultCommand implements CommandInterface
{
    /**
     * @Assert\NotNull
     */
    public float $weight;

    /**
     * @Assert\NotNull
     */
    public int $quantity;

    /**
     * @Assert\NotNull
     */
    public string $exerciseId;

    public function __construct(float $weight, int $quantity, string $exerciseId)
    {
        $this->weight = $weight;
        $this->quantity = $quantity;
        $this->exerciseId = $exerciseId;
    }
}