<?php


namespace App\Application\Sport\Result\Command\DeleteResult;


use App\Application\Core\CommandInterface;

final class DeleteResultCommand implements CommandInterface
{
    public string $resultId;

    public function __construct(string $resultId)
    {
        $this->resultId = $resultId;
    }
}