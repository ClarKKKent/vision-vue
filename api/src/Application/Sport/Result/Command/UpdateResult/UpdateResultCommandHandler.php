<?php


namespace App\Application\Sport\Result\Command\UpdateResult;


use App\Application\Core\CommandHandlerInterface;
use App\Domain\Core\Uuid;
use App\Domain\Sport\Repository\ExerciseRepositoryInterface;
use App\Domain\Sport\Repository\ResultRepositoryInterface;

final class UpdateResultCommandHandler implements CommandHandlerInterface
{
    private ResultRepositoryInterface $resultRepository;

    private ExerciseRepositoryInterface $exerciseRepository;

    public function __construct(
        ResultRepositoryInterface $resultRepository,
        ExerciseRepositoryInterface $exerciseRepository)
    {
        $this->resultRepository = $resultRepository;
        $this->exerciseRepository = $exerciseRepository;
    }

    public function __invoke(UpdateResultCommand $message): void
    {
        $result = $this->resultRepository->oneById(Uuid::fromString($message->resultId));
        $exercise = $this->exerciseRepository->oneById(Uuid::fromString($message->exerciseId));

        $result->update(
            $message->weight,
            $message->quantity
        );

        if (!$exercise->getResults()->contains($exercise)) {
            $exercise->applyResult($result);
        }

        $this->resultRepository->save($result);
    }
}