<?php


namespace App\Application\Sport\Result\Command\UpdateResult;


use App\Application\Core\CommandInterface;
use Symfony\Component\Validator\Constraints as Assert;

final class UpdateResultCommand implements CommandInterface
{
    /**
     * @Assert\NotNull
     */
    public string $resultId;

    /**
     * @Assert\NotNull
     */
    public float $weight;

    /**
     * @Assert\NotNull
     */
    public int $quantity;

    /**
     * @Assert\NotNull
     */
    public string $exerciseId;

    public function __construct(string $resultId, float $weight, int $quantity, string $exerciseId)
    {
        $this->resultId = $resultId;
        $this->weight = $weight;
        $this->quantity = $quantity;
        $this->exerciseId = $exerciseId;
    }
}