<?php


namespace App\Application\Sport\Result\Query\GetResult;


use App\Application\Core\QueryInterface;

final class GetResultQuery implements QueryInterface
{
    public string $id;

    public function __construct(string $id)
    {
        $this->id = $id;
    }
}