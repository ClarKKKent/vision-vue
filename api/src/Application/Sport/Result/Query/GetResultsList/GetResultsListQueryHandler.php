<?php


namespace App\Application\Sport\Result\Query\GetResultsList;


use App\Application\Core\PaginatedCollection;
use App\Application\Core\QueryHandlerInterface;
use App\Domain\User\Repository\UserRepositoryInterface;
use App\Infrastructure\Core\Security\IdentityFetcherInterface;
use App\Infrastructure\Sport\Persistence\Query\ResultCollectionInterface;

final class GetResultsListQueryHandler implements QueryHandlerInterface
{
    private ResultCollectionInterface $resultCollection;

    private UserRepositoryInterface $userRepository;

    private IdentityFetcherInterface $identityFetcher;

    public function __construct(
        ResultCollectionInterface $resultCollection,
        UserRepositoryInterface $userRepository,
        IdentityFetcherInterface $identityFetcher)
    {
        $this->resultCollection = $resultCollection;
        $this->userRepository = $userRepository;
        $this->identityFetcher = $identityFetcher;
    }

    public function __invoke(GetResultsListQuery $query): PaginatedCollection
    {
        $user = $this->userRepository->oneByIdentity($this->identityFetcher->get());

        $query->userId = $user->getId()->toString();

        $results = $this->resultCollection->list($query);

        return new PaginatedCollection($results);
    }
}