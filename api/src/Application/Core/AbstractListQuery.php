<?php


namespace App\Application\Core;


abstract class AbstractListQuery
{
    public ?int $page;

    public ?int $limit;

    public function __construct(?int $page = 1, ?int $limit = 10)
    {
        $this->page = $page;
        $this->limit = $limit;
    }
}