<?php


namespace App\Application\Core;


use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\Paginator;
use Knp\Component\Pager\PaginatorInterface;

final class PaginatedCollection
{
    private array $data;

    private PaginationInterface $resource;

    private PaginatorInterface $paginator;

    public function __construct(array $data, int $page = 1, int $size = 10)
    {
        $this->data = $data;
        $this->paginator = new Paginator();
        $this->resource = $this->paginator->paginate($this->data, $page, $size);
    }

    public function resource(): PaginationInterface
    {
        return $this->resource;
    }
}