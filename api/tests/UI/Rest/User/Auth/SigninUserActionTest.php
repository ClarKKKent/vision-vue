<?php


namespace App\Tests\UI\Rest\User\Auth;


use App\Infrastructure\Core\Persistence\Doctrine\Fixtures\User\User\UserForAuthFixture;
use App\Tests\UI\UITestCase;
use Symfony\Bundle\FrameworkBundle\Client;

class SigninUserActionTest extends UITestCase
{
    private ?Client $client;

    protected function setUp(): void
    {
        parent::setUp();
        $this->addFixture(new UserForAuthFixture());
        $this->executeFixtures();
        self::ensureKernelShutdown();
        $this->client = self::createClient();
    }

    public function testSuccess()
    {
        $this->client->request(
          'POST',
            '/api/v1/auth/signin',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode(array(
                'email' => 'test@test.com',
                'password' => 'password',
            ))
        );
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode());
    }

    public function testInvalidEmail()
    {
        $this->client->request(
            'POST',
            '/api/v1/auth/signin',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode(array(
                'email' => 'invalid@test.com',
                'password' => 'password'
            ))
        );
        $this->assertEquals(401, $this->client->getResponse()->getStatusCode());
    }

    public function testInvalidPassword()
    {
        $this->client->request(
            'POST',
            '/api/v1/auth/signin',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode(array(
                'email' => 'test@test.com',
                'password' => 'invalid'
            ))
        );
        $this->assertEquals(401, $this->client->getResponse()->getStatusCode());
    }
}