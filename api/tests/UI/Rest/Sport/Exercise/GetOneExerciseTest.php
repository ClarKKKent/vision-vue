<?php


namespace App\Tests\UI\Rest\Sport\Exercise;


use App\Infrastructure\Core\Persistence\Doctrine\Builders\Sport\Exercise\ExerciseBuilder;
use App\Infrastructure\Core\Persistence\Doctrine\Fixtures\Sport\Exercise\ExerciseFixture;
use App\Infrastructure\Core\Persistence\Doctrine\Fixtures\User\User\UserForAuthFixture;
use App\Tests\UI\UITestCase;
use Symfony\Bundle\FrameworkBundle\Client;

class GetOneExerciseTest extends UITestCase
{
    private ?Client $client;

    protected function setUp(): void
    {
        parent::setUp();

        $this->addFixture(new ExerciseFixture());
        $this->addFixture(new UserForAuthFixture());
        $this->executeFixtures();

        self::ensureKernelShutdown();

        $this->client = $this->createAuthenticatedClient();
    }

    public function testSuccess()
    {
        $this->client->request(
            'GET',
            '/api/v1/sport/exercises/' . ExerciseBuilder::EXERCISE_TEST_ID,
            [],
            [],
            ['CONTENT_TYPE' => 'application/json']
        );

        self::assertEquals(200, $this->client->getResponse()->getStatusCode());
    }
}