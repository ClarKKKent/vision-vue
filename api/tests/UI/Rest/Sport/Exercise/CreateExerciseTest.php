<?php


namespace App\Tests\UI\Rest\Sport\Exercise;


use App\Infrastructure\Core\Persistence\Doctrine\Fixtures\User\User\UserForAuthFixture;
use App\Tests\UI\UITestCase;
use Symfony\Bundle\FrameworkBundle\Client;

class CreateExerciseTest extends UITestCase
{
    private Client $client;

    protected function setUp(): void
    {
        parent::setUp();
        $this->addFixture(new UserForAuthFixture());
        $this->executeFixtures();
        self::ensureKernelShutdown();
        $this->client = $this->createAuthenticatedClient();
    }

    public function testSuccess()
    {
        $this->client->request(
            'POST',
            '/api/v1/sport/exercises',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'title' => 'titles',
                'description' => 'description'
            ])
        );

        $this->assertEquals(201, $this->client->getResponse()->getStatusCode());
    }
}