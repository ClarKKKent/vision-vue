<?php


namespace App\Tests\UI\Rest\Sport\Exercise;


use App\Infrastructure\Core\Persistence\Doctrine\Fixtures\Sport\Exercise\ExerciseFixture;
use App\Infrastructure\Core\Persistence\Doctrine\Fixtures\User\User\UserForAuthFixture;
use App\Tests\UI\UITestCase;
use Symfony\Bundle\FrameworkBundle\Client;

class DeleteExerciseTest extends UITestCase
{
    private Client $client;

    protected function setUp(): void
    {
        parent::setUp();

        $this->addFixture(new UserForAuthFixture());
        $this->addFixture(new ExerciseFixture());
        $this->executeFixtures();

        self::ensureKernelShutdown();

        $this->client = $this->createAuthenticatedClient();
    }

    public function testSuccess()
    {
        $this->client->request(
            'DELETE',
            '/api/v1/sport/exercises/fb3c19ff-584d-419c-a72e-a314b0056b65',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([])
        );

        self::assertEquals(204, $this->client->getResponse()->getStatusCode());
    }
}