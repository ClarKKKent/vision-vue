<?php


namespace App\Tests\UI\Rest\Sport\Result;


use App\Infrastructure\Core\Persistence\Doctrine\Builders\Sport\Result\ResultBuilder;
use App\Infrastructure\Core\Persistence\Doctrine\Fixtures\Sport\Result\ResultFixture;
use App\Infrastructure\Core\Persistence\Doctrine\Fixtures\User\User\UserForAuthFixture;
use App\Tests\UI\UITestCase;
use Symfony\Bundle\FrameworkBundle\Client;

class DeleteResultTest extends UITestCase
{
    private ?Client $client;

    protected function setUp(): void
    {
        parent::setUp();

        $this->addFixture(new UserForAuthFixture());
        $this->addFixture(new ResultFixture());
        $this->executeFixtures();

        self::ensureKernelShutdown();

        $this->client = $this->createAuthenticatedClient();
    }

    public function testSuccess()
    {
        $this->client->request(
            'DELETE',
            '/api/v1/sport/results/' . ResultBuilder::RESULT_TEST_ID,
            [],
            [],
            ['CONTENT_TYPE' => 'application/json']
        );

        self::assertEquals(204, $this->client->getResponse()->getStatusCode());
    }
}