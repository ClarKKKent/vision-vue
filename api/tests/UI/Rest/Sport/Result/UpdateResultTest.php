<?php


namespace App\Tests\UI\Rest\Sport\Result;


use App\Infrastructure\Core\Persistence\Doctrine\Builders\Sport\Exercise\ExerciseBuilder;
use App\Infrastructure\Core\Persistence\Doctrine\Builders\Sport\Result\ResultBuilder;
use App\Infrastructure\Core\Persistence\Doctrine\Fixtures\Sport\Exercise\ExerciseFixture;
use App\Infrastructure\Core\Persistence\Doctrine\Fixtures\Sport\Result\ResultFixture;
use App\Infrastructure\Core\Persistence\Doctrine\Fixtures\User\User\UserForAuthFixture;
use App\Tests\UI\UITestCase;
use Symfony\Bundle\FrameworkBundle\Client;


class UpdateResultTest extends UITestCase
{
    private ?Client $client;

    protected function setUp(): void
    {
        parent::setUp();

        $this->addFixture(new UserForAuthFixture());
        $this->addFixture(new ResultFixture());
        $this->addFixture(new ExerciseFixture());
        $this->executeFixtures();

        self::ensureKernelShutdown();

        $this->client = $this->createAuthenticatedClient();
    }

    public function testSuccess()
    {
        $this->client->request(
            'PUT',
            '/api/v1/sport/results/' . ResultBuilder::RESULT_TEST_ID,
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode([
                'weight' => 10.0,
                'quantity' => 5,
                'exercise_id' => ExerciseBuilder::EXERCISE_TEST_ID
            ])
        );

        self::assertEquals(204, $this->client->getResponse()->getStatusCode());
    }
}