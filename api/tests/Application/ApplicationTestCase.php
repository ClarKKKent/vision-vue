<?php


namespace App\Tests\Application;


use App\Application\Core\CommandInterface;
use App\Application\Core\QueryInterface;
use App\Infrastructure\Core\Bus\CommandBusInterface;
use App\Infrastructure\Core\Bus\QueryBusInterface;
use App\Tests\FixtureAwareTestCase;

class ApplicationTestCase extends FixtureAwareTestCase
{
    private ?CommandBusInterface $commandBus;

    private ?QueryBusInterface $queryBus;

    protected function setUp(): void
    {
        parent::setUp();
        $this->commandBus = $this->service(CommandBusInterface::class);
        $this->queryBus = $this->service(QueryBusInterface::class);
    }

    protected function ask(QueryInterface $query)
    {
        return $this->queryBus->handle($query);
    }

    protected function exec(CommandInterface $command): void
    {
        $this->commandBus->handle($command);
    }

    protected function service(string $serviceId): ?object
    {
        return self::$container->get($serviceId);
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        $this->commandBus = null;
        $this->queryBus = null;
    }
}