<?php


namespace App\Tests\Domain\User\Entity;


use App\Domain\Core\Uuid;
use App\Domain\User\Entity\User;
use App\Domain\User\Specification\UniqueEmailSpecificationInterface;
use App\Domain\User\ValueObject\Email;
use App\Infrastructure\Core\Security\Password\Bcrypt\BcryptPasswordHasher;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testSuccess()
    {
        $stub = $this->createMock(UniqueEmailSpecificationInterface::class);
        $stub
            ->method('isSatisfiedBy')
            ->willReturn(true);
        $user = User::create(
          new Uuid($id = \Ramsey\Uuid\Uuid::uuid4()),
          $email = Email::fromString($email = 'test@test.com'),
          (new BcryptPasswordHasher())->hash($password = 'password'),
          $stub
        );

        self::assertEquals($id, $user->getId()->toString());
        self::assertEquals($email, $user->getEmail()->toString());
        self::assertNotEquals($password, $user->getPasswordHash());
    }
}