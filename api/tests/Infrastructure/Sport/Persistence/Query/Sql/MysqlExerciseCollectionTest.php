<?php


namespace App\Tests\Infrastructure\Sport\Persistence\Query\Sql;


use App\Application\Sport\Exercise\Query\GetExercise\GetExerciseQuery;
use App\Application\Sport\Exercise\Query\GetExercisesList\GetExercisesListQuery;
use App\Infrastructure\Core\Persistence\Doctrine\Builders\Sport\Exercise\ExerciseBuilder;
use App\Infrastructure\Core\Persistence\Doctrine\Builders\User\User\UserBuilder;
use App\Infrastructure\Core\Persistence\Doctrine\Fixtures\Sport\Exercise\ExerciseFixture;
use App\Infrastructure\Core\Persistence\Doctrine\Fixtures\Sport\Exercise\ExercisesFixture;
use App\Infrastructure\Sport\Persistence\Query\ExerciseCollectionInterface;
use App\Infrastructure\Sport\Persistence\Query\Mysql\MysqlExerciseCollection;
use App\Tests\FixtureAwareTestCase;
use Doctrine\ORM\EntityManagerInterface;

class MysqlExerciseCollectionTest extends FixtureAwareTestCase
{
    private ?EntityManagerInterface $entityManager;

    private ?ExerciseCollectionInterface $exerciseCollection;

    protected function setUp(): void
    {
        parent::setUp();

        $kernel = static::bootKernel();
        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
        $this->exerciseCollection = new MysqlExerciseCollection($this->entityManager);
    }

    public function testListSuccess()
    {
        $this->addFixture(new ExercisesFixture());
        $this->executeFixtures();

        $query = new GetExercisesListQuery();
        $query->userId = UserBuilder::USER_TEST_ID;

        $result = $this->exerciseCollection->list($query);

        self::assertCount(3, $result);
    }

    public function testOneSuccess()
    {
        $this->addFixture(new ExerciseFixture());
        $this->executeFixtures();

        $result = $this->exerciseCollection->one(ExerciseBuilder::EXERCISE_TEST_ID);

        self::assertEquals(ExerciseBuilder::EXERCISE_TEST_ID, $result->id);
    }
}