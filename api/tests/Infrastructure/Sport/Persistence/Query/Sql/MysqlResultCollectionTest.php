<?php


namespace App\Tests\Infrastructure\Sport\Persistence\Query\Sql;


use App\Application\Sport\Result\Query\GetResult\GetResultQuery;
use App\Application\Sport\Result\Query\GetResultsList\GetResultsListQuery;
use App\Infrastructure\Core\Persistence\Doctrine\Builders\Sport\Result\ResultBuilder;
use App\Infrastructure\Core\Persistence\Doctrine\Builders\User\User\UserBuilder;
use App\Infrastructure\Core\Persistence\Doctrine\Fixtures\Sport\Result\ResultFixture;
use App\Infrastructure\Core\Persistence\Doctrine\Fixtures\Sport\Result\ResultsFixture;
use App\Infrastructure\Sport\Persistence\Query\Mysql\MysqlResultCollection;
use App\Infrastructure\Sport\Persistence\Query\ResultCollectionInterface;
use App\Tests\FixtureAwareTestCase;
use Doctrine\ORM\EntityManagerInterface;

class MysqlResultCollectionTest extends FixtureAwareTestCase
{
    private ?EntityManagerInterface $entityManager;

    private ?ResultCollectionInterface $resultCollection;

    protected function setUp(): void
    {
        parent::setUp();

        $kernel = self::bootKernel();
        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
        $this->resultCollection = new MysqlResultCollection($this->entityManager);
    }

    public function testListSuccess()
    {
        $this->addFixture(new ResultsFixture());
        $this->executeFixtures();

        $query = new GetResultsListQuery();
        $query->userId = UserBuilder::USER_TEST_ID;

        $results = $this->resultCollection->list($query);

        self::assertCount(ResultsFixture::RESULTS_LENGTH, $results);
    }

    public function testOneSuccess()
    {
        $this->addFixture(new ResultFixture());
        $this->executeFixtures();

        $query = new GetResultQuery(ResultBuilder::RESULT_TEST_ID);

        $result = $this->resultCollection->one($query);

        self::assertNotNull($result);
        self::assertEquals(ResultBuilder::RESULT_TEST_ID, $result->id);
    }
}