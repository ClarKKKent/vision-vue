<?php


namespace App\Tests\Infrastructure\Sport\Persistence\Store\Sql;


use App\Domain\Core\Uuid;
use App\Domain\Sport\Entity\Result;
use App\Infrastructure\Core\Persistence\Doctrine\Builders\Sport\Result\ResultBuilder;
use App\Infrastructure\Core\Persistence\Doctrine\Fixtures\Sport\Result\ResultFixture;
use App\Infrastructure\Sport\Persistence\Store\Mysql\MysqlResultRepository;
use App\Tests\FixtureAwareTestCase;
use Doctrine\ORM\EntityManagerInterface;

class MysqlResultRepositoryTest extends FixtureAwareTestCase
{
    private ?EntityManagerInterface $entityManager;

    private ?MysqlResultRepository $resultRepository;

    protected function setUp(): void
    {
        parent::setUp();

        $kernel = self::bootKernel();
        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
        $this->resultRepository = new MysqlResultRepository($this->entityManager);
    }

    public function testSaveSuccess()
    {
        $result = ResultBuilder::any();
        $this->resultRepository->save($result);

        $existingResult = $this->entityManager->find(Result::class, ResultBuilder::RESULT_TEST_ID);

        self::assertNotNull($existingResult);
        self::assertEquals($existingResult->getId(), $result->getId());
    }

    public function testDeleteSuccess()
    {
        $this->executeExerciseFixtures();

        /** @var Result $result */
        $result = $this->entityManager->find(Result::class, ResultBuilder::RESULT_TEST_ID);
        $this->resultRepository->delete($result);

        $maybeResult = $this->entityManager->find(Result::class, ResultBuilder::RESULT_TEST_ID);

        self::assertNull($maybeResult);
    }

    public function testOneByIdSuccess()
    {
        $this->executeExerciseFixtures();

        $result = $this->resultRepository->oneById(Uuid::fromString(ResultBuilder::RESULT_TEST_ID));

        self::assertEquals(ResultBuilder::RESULT_TEST_ID, $result->getId()->toString());
    }

    private function executeExerciseFixtures()
    {
        $this->addFixture(new ResultFixture());
        $this->executeFixtures();
    }
}