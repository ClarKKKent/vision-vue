<?php


namespace App\Tests\Infrastructure\Sport\Persistence\Store\Sql;


use App\Domain\Core\Uuid;
use App\Domain\Sport\Entity\Exercise;
use App\Infrastructure\Core\Persistence\Doctrine\Builders\Sport\Exercise\ExerciseBuilder;
use App\Infrastructure\Core\Persistence\Doctrine\Fixtures\Sport\Exercise\ExerciseFixture;
use App\Infrastructure\Sport\Persistence\Store\Mysql\MysqlExerciseRepository;
use App\Tests\FixtureAwareTestCase;
use Doctrine\ORM\EntityManagerInterface;


class MysqlExerciseRepositoryTest extends FixtureAwareTestCase
{
    private ?EntityManagerInterface $entityManager;

    private ?MysqlExerciseRepository $exerciseRepository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->addFixture(new ExerciseFixture());
        $this->executeFixtures();

        $kernel = static::bootKernel();

        $this->entityManager = $kernel->getContainer()->get('doctrine')->getManager();
        $this->exerciseRepository = new MysqlExerciseRepository($this->entityManager);
    }

    public function testSaveSuccess()
    {
        $exercise = Exercise::create(
            $id = new Uuid(\Ramsey\Uuid\Uuid::uuid4()),
            'test',
            'test'
        );
        $this->exerciseRepository->save($exercise);

        $exerciseFromDB = $this->entityManager->find(Exercise::class, $id->toString());

        self::assertEquals($exercise->getId()->toString(), $exerciseFromDB->getId()->toString());
    }

    public function testDeleteSuccess()
    {
        $exercise = $this->entityManager->find(Exercise::class, ExerciseBuilder::EXERCISE_TEST_ID);

        $this->exerciseRepository->delete($exercise);

        $maybeExercise = $this->entityManager->find(Exercise::class, ExerciseBuilder::EXERCISE_TEST_ID);

        self::assertNull($maybeExercise);
    }

    public function testOneByIdSuccess()
    {
        $exercise = $this->exerciseRepository->oneById(new Uuid('fb3c19ff-584d-419c-a72e-a314b0056b65'));

        self::assertNotNull($exercise);
    }
}