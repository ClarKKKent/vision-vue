const fs = require('fs');
const path = require('path');
const chalk = require('chalk');

const error = (...args) => {
  console.error(chalk.red(...args));
};

const success = (...args) => {
  console.log(chalk.green(...args));
};

const args = process.argv.slice(2);

if (!args.length) {
  error('You must provide a name for the module.');
  return;
}

const domainName = args[0];
const moduleName = args[1];

const domainPath = path.join(__dirname, `../src/${domainName}/store`);

if (!fs.existsSync(domainPath)) {
  fs.mkdirSync(domainPath);
}

const fullPath = path.join(
  __dirname,
  `../src/${domainName}/store/${moduleName}`
);

if (fs.existsSync(fullPath)) {
  error('Store module already exist');
  process.exit(1);
}

const indexContent = `import state from './state';
import getters from './getters';
import actions from './actions';
import mutations from './mutations';

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
`;

const stateContent = `const state = () => ({});

export default state;
`;

const gettersContent = `const getters = {};

export default getters;
`;

const mutationsContent = `import * as types from './types';

const mutations = {};

export default mutations;
`;

const actionsContent = `import * as types from './types';

const actions = {};

export default actions;
`;

const indexPath = `${path.join(fullPath, 'index.js')}`;
const statePath = `${path.join(fullPath, 'state.js')}`;
const gettersPath = `${path.join(fullPath, 'getters.js')}`;
const mutationsPath = `${path.join(fullPath, 'mutations.js')}`;
const actionsPath = `${path.join(fullPath, 'actions.js')}`;
const typesPath = `${path.join(fullPath, 'types.js')}`;

fs.mkdirSync(fullPath);
fs.appendFileSync(indexPath, indexContent);
fs.appendFileSync(statePath, stateContent);
fs.appendFileSync(gettersPath, gettersContent);
fs.appendFileSync(mutationsPath, mutationsContent);
fs.appendFileSync(actionsPath, actionsContent);
fs.appendFileSync(typesPath, '');

success('Module successfully generated!');
