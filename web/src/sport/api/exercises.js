import api from '@/core/api';

function fetchExercise(id) {
  return api(`/api/v1/sport/exercises/${id}`, { method: 'GET' });
}

function fetchExercisesList() {
  return api('/api/v1/sport/exercises', { method: 'GET' });
}

function createExercise(data) {
  return api('/api/v1/sport/exercises', { method: 'POST', data });
}

function updateExercise({ id, data }) {
  return api(`/api/v1/sport/exercises/${id}`, { method: 'PUT', data });
}

function deleteExercise(id) {
  return api(`/api/v1/sport/exercises/${id}`, { method: 'DELETE' });
}

export {
  fetchExercise,
  fetchExercisesList,
  createExercise,
  updateExercise,
  deleteExercise
};
