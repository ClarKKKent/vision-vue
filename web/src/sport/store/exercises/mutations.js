import Vue from 'vue';

import * as types from './types';

const mutations = {
  [types.SET_EXERCISES](state, payload) {
    state.exercises = payload.items ? payload.items : payload;
    state.exercises = state.exercises.map((exercise) => {
      Vue.set(exercise, 'loading', false);
      return exercise;
    });
    state.exercisesMeta = payload.meta ? payload.meta : null;
  },

  [types.SET_EXERCISE](state, payload) {
    state.exercise = payload;
  },

  [types.SET_FETCH_EXERCISES_API_STATUS](state, payload) {
    state.fetchExercisesApiStatus = payload;
  },

  [types.SET_FETCH_EXERCISE_API_STATUS](state, payload) {
    state.fetchExerciseApiStatus = payload;
  },

  [types.SET_MUTATE_EXERCISES_API_STATUS](state, payload) {
    state.mutateExercisesApiStatus = payload;
  },

  [types.SET_ERROR](state, payload) {
    state.error = payload;
  }
};

export default mutations;
