import mutateAsync from '@/core/api/mutate-async';
import queryAsync from '@/core/api/query-async';
import { apiStatus } from '@/core/constants/api';
import {
  createExercise,
  deleteExercise,
  fetchExercisesList,
  updateExercise
} from '@/sport/api/exercises';

import { fetchExercise } from '../../api/exercises';
import * as types from './types';

const actions = {
  async fetchOne({ commit }, id) {
    commit(types.SET_FETCH_EXERCISE_API_STATUS, apiStatus.PENDING);
    const { response, error } = await queryAsync({
      fn: fetchExercise,
      params: id
    });
    if (error) {
      commit(types.SET_FETCH_EXERCISE_API_STATUS, apiStatus.ERROR);
      commit(types.SET_ERROR, error);
      return;
    }
    commit(types.SET_FETCH_EXERCISES_API_STATUS, apiStatus.SUCCESS);
    commit(types.SET_EXERCISE, response.data);
  },

  async fetchList({ commit }, params) {
    commit(types.SET_FETCH_EXERCISES_API_STATUS, apiStatus.PENDING);
    const { response, error } = await queryAsync({
      fn: fetchExercisesList,
      params
    });
    if (error) {
      commit(types.SET_FETCH_EXERCISES_API_STATUS, apiStatus.ERROR);
      return;
    }
    commit(types.SET_EXERCISES, response.data);
    commit(types.SET_FETCH_EXERCISES_API_STATUS, apiStatus.SUCCESS);
  },

  async add({ commit }, data) {
    commit(types.SET_MUTATE_EXERCISES_API_STATUS, apiStatus.PENDING);
    const { error } = await mutateAsync({ fn: createExercise, data });
    if (error) {
      commit(types.SET_MUTATE_EXERCISES_API_STATUS, apiStatus.ERROR);
      commit(types.SET_ERROR, error);
      return;
    }
    commit(types.SET_MUTATE_EXERCISES_API_STATUS, apiStatus.SUCCESS);
    commit(types.SET_ERROR, null);
  },

  async update({ commit }, data) {
    commit(types.SET_MUTATE_EXERCISES_API_STATUS, apiStatus.PENDING);
    const { error } = await mutateAsync({ fn: updateExercise, data });
    if (error) {
      commit(types.SET_MUTATE_EXERCISES_API_STATUS, apiStatus.ERROR);
      commit(types.SET_ERROR, error);
      return;
    }
    commit(types.SET_MUTATE_EXERCISES_API_STATUS, apiStatus.SUCCESS);
    commit(types.SET_ERROR, null);
  },

  async delete({ commit }, id) {
    commit(types.SET_MUTATE_EXERCISES_API_STATUS, apiStatus.PENDING);
    const { error } = await mutateAsync({ fn: deleteExercise, data: id });
    if (error) {
      commit(types.SET_MUTATE_EXERCISES_API_STATUS, apiStatus.ERROR);
      commit(types.SET_ERROR, error);
      return;
    }
    commit(types.SET_MUTATE_EXERCISES_API_STATUS, apiStatus.SUCCESS);
    commit(types.SET_ERROR, null);
  }
};

export default actions;
