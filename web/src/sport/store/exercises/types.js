// Mutations
export const SET_EXERCISES = 'SET_EXERCISES';

export const SET_EXERCISE = 'SET_EXERCISE';

export const SET_FETCH_EXERCISES_API_STATUS = 'SET_FETCH_EXERCISES_API_STATUS';

export const SET_FETCH_EXERCISE_API_STATUS = 'SET_FETCH_EXERCISE_API_STATUS';

export const SET_MUTATE_EXERCISES_API_STATUS =
  'SET_MUTATE_EXERCISES_API_STATUS';

export const SET_ERROR = 'SET_ERROR';

//Actions
export const fetchExercise = 'exercises/fetchOne';

export const fetchExercises = 'exercises/fetchList';

export const addExercise = 'exercises/add';

export const updateExercise = 'exercises/update';

export const deleteExercise = 'exercises/delete';

//Getters
export const getExercise = 'exercises/getExercise';

export const getExercises = 'exercises/getExercises';

export const getFetchExercisesApiStatus =
  'exercises/getFetchExercisesApiStatus';

export const getFetchExerciseApiStatus = 'exercises/getFetchExerciseApiStatus';

export const getMutateExercisesApiStatus =
  'exercises/getMutateExercisesApiStatus';

export const getError = 'exercises/getError';
