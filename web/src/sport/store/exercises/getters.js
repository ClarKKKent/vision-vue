const getters = {
  getExercises: (state) => state.exercises,
  getExercise: (state) => state.exercise,
  getFetchExercisesApiStatus: (state) => state.fetchExercisesApiStatus,
  getFetchExerciseApiStatus: (state) => state.fetchExerciseApiStatus,
  getMutateExercisesApiStatus: (state) => state.mutateExercisesApiStatus,
  getError: (state) => state.error
};

export default getters;
