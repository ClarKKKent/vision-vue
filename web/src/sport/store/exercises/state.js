import { apiStatus } from '@/core/constants/api';

const state = () => ({
  exercises: [],
  exercisesMeta: null,
  exercise: null,
  error: null,
  fetchExercisesApiStatus: apiStatus.IDLE,
  fetchExerciseApiStatus: apiStatus.IDLE,
  mutateExercisesApiStatus: apiStatus.IDLE
});

export default state;
