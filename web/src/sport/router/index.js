import exercisesRoutes from '@/sport/router/exercises';

const sportRoutes = [...exercisesRoutes];

export default sportRoutes;
