const exercisesRoutes = [
  {
    path: '/sport/exercises',
    name: 'sport-exercises',
    component: () => import('@/sport/views/exercises/ExercisesView')
  }
];

export default exercisesRoutes;
