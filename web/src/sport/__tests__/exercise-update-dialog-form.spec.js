import { createLocalVue, mount } from '@vue/test-utils';
import Vue from 'vue';
import Vuetify from 'vuetify';

import ExerciseUpdateDialogForm from '@/sport/components/exercises/ExerciseUpdateDialogForm';

const localVue = createLocalVue();
Vue.use(Vuetify);

describe('ExerciseUpdateDialogForm.vue', () => {
  let wrapper;
  let vuetify;

  beforeEach(() => {
    vuetify = new Vuetify();
    wrapper = mount(ExerciseUpdateDialogForm, {
      localVue,
      vuetify,
      propsData: {
        exercise: {
          id: '1',
          title: 'title',
          description: 'description'
        },
        loading: false
      }
    });
  });

  it('should render correct form values', () => {
    expect(wrapper.find('input').element.value).toEqual('title');
    expect(wrapper.find('textarea').element.value).toEqual('description');
  });

  it('should be emitted event when submitted', () => {
    wrapper.find('form').trigger('submit');

    expect(wrapper.emitted()).toBeTruthy();
    expect(wrapper.emitted().submit[0]).toEqual([
      {
        title: 'title',
        description: 'description'
      }
    ]);
  });
});
