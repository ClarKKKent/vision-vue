import { createLocalVue, mount } from '@vue/test-utils';
import Vue from 'vue';
import Vuetify from 'vuetify';
import Vuex from 'vuex';

import ExercisesList from '@/sport/components/exercises/ExercisesList';

const localVue = createLocalVue();
localVue.use(Vuex);
Vue.use(Vuetify);

describe('ExercisesList.vue', () => {
  let vuetify;

  beforeEach(() => {
    vuetify = new Vuetify();
  });
  it('should correct render exercises', () => {
    const wrapper = mount(ExercisesList, {
      localVue,
      vuetify,
      stubs: ['exercise-add-dialog', 'exercise-delete-dialog'],
      propsData: {
        exercises: [
          {
            id: '1',
            title: 'title1',
            description: 'description1'
          },
          {
            id: '2',
            title: 'title2',
            description: 'description2'
          }
        ]
      }
    });

    expect(wrapper.findAll('.v-card').length).toBe(2);
    expect(
      wrapper
        .findAll('.v-card')
        .at(0)
        .html()
    ).toContain('title1');
  });
});
