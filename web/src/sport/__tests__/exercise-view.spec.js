import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vue from 'vue';
import Vuetify from 'vuetify';
import Vuex from 'vuex';

import ExercisesView from '@/sport/views/exercises/ExercisesView';

const localVue = createLocalVue();

localVue.use(Vuex);
Vue.use(Vuetify);

describe('ExercisesView.vue', () => {
  let store;
  let vuetify;
  let actions;

  beforeEach(() => {
    actions = {
      fetchList: jest.fn()
    };
    vuetify = new Vuetify();
    store = new Vuex.Store({
      modules: {
        exercises: {
          namespaced: true,
          getters: {
            getExercises: () => []
          },
          actions
        }
      }
    });
  });

  it('should call action when created', () => {
    shallowMount(ExercisesView, {
      localVue,
      store,
      vuetify
    });

    expect(actions.fetchList).toHaveBeenCalled();
  });
});
