import api from '@/core/api';

function fetchUser() {
  return api('/api/v1/auth/me', { method: 'GET' });
}

export default fetchUser;
