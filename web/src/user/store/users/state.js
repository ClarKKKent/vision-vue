import { apiStatus } from '@/core/constants/api';

const state = () => ({
  user: null,
  fetchUserApiStatus: apiStatus.IDLE
});

export default state;
