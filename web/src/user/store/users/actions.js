import queryAsync from '@/core/api/query-async';
import { apiStatus } from '@/core/constants/api';
import fetchUser from '@/user/api/users';

import * as types from './types';

const actions = {
  async fetchUser({ commit }) {
    commit(types.SET_FETCH_USER_API_STATUS, apiStatus.PENDING);
    const { response, error } = await queryAsync({
      fn: fetchUser
    });
    if (error) {
      commit(types.SET_FETCH_USER_API_STATUS, apiStatus.ERROR);
      return false;
    }
    commit(types.SET_USER, response.data);
    commit(types.SET_FETCH_USER_API_STATUS, apiStatus.SUCCESS);
    return true;
  }
};

export default actions;
