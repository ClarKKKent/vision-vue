export const SET_FETCH_USER_API_STATUS = 'SET_FETCH_USER_API_STATUS';

export const SET_USER = 'SET_USER';

export const fetchUser = 'users/fetchUser';

export const getUser = 'users/getUser';

export const getFetchUserApiStatus = 'users/getFetchUserApiStatus';
