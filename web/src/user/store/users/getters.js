const getters = {
  getUser: (state) => state.user,
  getFetchUserApiStatus: (state) => state.fetchUserApiStatus
};

export default getters;
