import * as types from './types';

const mutations = {
  [types.SET_USER](state, payload) {
    state.user = payload;
  },
  [types.SET_FETCH_USER_API_STATUS](state, payload) {
    state.fetchUserApiStatus = payload;
  }
};

export default mutations;
