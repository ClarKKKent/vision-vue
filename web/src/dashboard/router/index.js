import DashboardView from '@/dashboard/views/DashboardView';

const dashboardRoutes = [
  {
    path: '',
    name: 'dashboard',
    component: DashboardView
  }
];

export default dashboardRoutes;
