import { upperFirst } from 'lodash-es';

import { apiStatus } from '@/core/constants/api';

function apiStatusComputedFactory(reactiveKeys = []) {
  let computed = {};

  const properties = Array.isArray(reactiveKeys)
    ? reactiveKeys
    : [reactiveKeys];

  properties.forEach((reactivePropertyKey) => {
    for (const [statusKey, statusValue] of Object.entries(apiStatus)) {
      const normalizedStatus = upperFirst(statusKey.toLowerCase());

      computed[`${reactivePropertyKey}${normalizedStatus}`] = function() {
        return this[reactivePropertyKey] === statusValue;
      };
    }
  });

  return computed;
}

export default apiStatusComputedFactory;
