import { createLocalVue, mount } from '@vue/test-utils';
import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuetify from 'vuetify';

import TheNavigation from '@/core/components/layout/TheNavigation';

const localVue = createLocalVue();
localVue.use(VueRouter);
Vue.use(Vuetify);

describe('TheNavigation.vue', () => {
  let vuetify;
  let wrapper;
  let router;

  beforeEach(() => {
    router = new VueRouter({
      mode: 'history'
    });

    vuetify = new Vuetify();

    wrapper = mount(TheNavigation, {
      localVue,
      router,
      vuetify,
      propsData: {
        appTitle: 'appTitle',
        appVersion: '1.0.0',
        navItems: [
          {
            title: 'Упражнения',
            url: '/sport/exercises'
          }
        ]
      }
    });
  });

  it('should render text props', () => {
    expect(wrapper.html()).toContain('appTitle');
    expect(wrapper.html()).toContain('1.0.0');
  });

  it('should render navItems', () => {
    expect(wrapper.findAll('.v-list-item--link').length).toBe(1);
  });
});
