import { createLocalVue, mount } from '@vue/test-utils';
import Vue from 'vue';
import Vuetify from 'vuetify';

import TheNavigationLink from '@/core/components/layout/TheNavigationLink';

const localVue = createLocalVue();
Vue.use(Vuetify);

describe('TheNavigationLink.vue', () => {
  let wrapper;
  let router;
  let vuetify;

  beforeEach(() => {
    vuetify = new Vuetify();

    wrapper = mount(TheNavigationLink, {
      localVue,
      router,
      vuetify,
      propsData: {
        item: {
          title: 'Тест'
        }
      }
    });
  });

  it('should correct render title', () => {
    expect(wrapper.html()).toContain('Тест');
  });
});
