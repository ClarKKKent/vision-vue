import { createLocalVue, shallowMount } from '@vue/test-utils';
import Vue from 'vue';
import Vuetify from 'vuetify';
import Vuex from 'vuex';

import TheHeader from '@/core/components/layout/TheHeader';

const localVue = createLocalVue();
localVue.use(Vuex);
Vue.use(Vuetify);

describe('TheHeader.vue', () => {
  let actions;
  let getters;
  let store;
  let vuetify;
  let wrapper;

  beforeEach(() => {
    vuetify = new Vuetify();

    getters = {
      getUser: () => ({
        email: 'test@test.com'
      })
    };

    actions = {
      logout: jest.fn()
    };

    store = new Vuex.Store({
      modules: {
        auth: {
          namespaced: true,
          actions
        },
        users: {
          namespaced: true,
          getters
        }
      }
    });

    wrapper = shallowMount(TheHeader, {
      localVue,
      store,
      vuetify
    });
  });

  it('should called logout action on button click', async () => {
    wrapper
      .findAllComponents({ name: 'v-list-item' })
      .at(1)
      .vm.$emit('click');

    expect(actions.logout.mock.calls).toHaveLength(1);
  });
});
