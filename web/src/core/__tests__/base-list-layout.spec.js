import { mount } from '@vue/test-utils';

import BaseListLayout from '@/core/components/base/list/BaseListLayout';

describe('BaseListLayout.vue', () => {
  it('should be render correct slot when list', () => {
    const wrapper = mount(BaseListLayout, {
      propsData: {
        list: ['test']
      },
      slots: {
        list: '<ul>list</ul>',
        noList: '<div>noList</div>'
      }
    });

    expect(wrapper.html()).toContain('list');
    expect(wrapper.html()).not.toContain('noList');
  });

  it('should be render correct slot when no list', () => {
    const wrapper = mount(BaseListLayout, {
      propsData: {
        list: []
      },
      slots: {
        list: '<ul>list</ul>',
        noList: '<div>noList</div>'
      }
    });

    expect(wrapper.html()).not.toContain('list');
    expect(wrapper.html()).toContain('noList');
  });
});
