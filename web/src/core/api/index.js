import axios from 'axios';

import * as authProvider from '@/auth/utils/auth';
import store from '@/core/store';

const baseURL =
  process.env.NODE_ENV === 'development' ? 'http://localhost:8081' : '/';

async function api(
  endpoint,
  { method, data, params = {}, headers: customHeaders, ...customConfig }
) {
  const config = {
    method,
    url: `${baseURL}${endpoint}`,
    data: data ? data : undefined,
    params: params ? params : undefined,
    headers: {
      Content_type: data ? 'application/json' : undefined,
      ...customHeaders
    },
    ...customConfig
  };

  return axios(config)
    .then(async (response) => {
      if (response.status === 401) {
        await store.dispatch('auth/logout');
        authProvider.removeToken();

        return Promise.reject({ message: 'Please reauthenticate' });
      }
      return response;
    })
    .catch((error) => {
      return Promise.reject(error);
    });
}

export default api;
