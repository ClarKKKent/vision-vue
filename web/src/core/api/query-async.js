function queryAsync({ fn, params }, ...args) {
  return fn(params, ...args)
    .then((response) => ({
      response,
      error: null
    }))
    .catch((error) => ({
      response: null,
      error
    }));
}

export default queryAsync;
