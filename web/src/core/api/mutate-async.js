function mutateAsync({ fn, data }) {
  return fn(data)
    .then(() => ({
      error: null
    }))
    .catch((err) => ({
      error: err
    }));
}

export default mutateAsync;
