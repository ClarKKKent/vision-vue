import Vue from 'vue';
import VueRouter from 'vue-router';

import runMiddleware from '@/core/router/middlewares';
import authMiddleware from '@/core/router/middlewares/auth';
import routes from '@/core/router/routes';

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

runMiddleware(authMiddleware, router);

export default router;
