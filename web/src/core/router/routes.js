import authRoutes from '@/auth/router';
import AppLayout from '@/core/layouts/components/AppLayout';
import dashboardRoutes from '@/dashboard/router';
import sportRoutes from '@/sport/router';

const appLayoutRoutes = [...dashboardRoutes, ...sportRoutes];

const authLayoutRoutes = [...authRoutes];

const routes = [
  {
    path: '/',
    component: AppLayout,
    children: [...appLayoutRoutes]
  },
  {
    path: '/auth',
    component: () => import('@/core/layouts/components/AuthLayout'),
    children: [...authLayoutRoutes]
  }
];

export default routes;
