export default function runMiddleware(middleware, router) {
  return middleware(router);
}
