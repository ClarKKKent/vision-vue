import * as authProvider from '@/auth/utils/auth';

function authMiddleware(router) {
  router.beforeEach((to, from, next) => {
    const isPublic = to.matched.some((record) => record.meta.public);
    const onlyWhenLoggedOut = to.matched.some(
      (record) => record.meta.onlyWhenLoggedOut
    );
    const loggedIn = !!authProvider.getToken();

    if (!isPublic && !loggedIn) {
      return next({
        name: 'auth-signin',
        query: { redirect: to.fullPath }
      });
    }

    if (loggedIn && onlyWhenLoggedOut) {
      return next({ name: 'dashboard' });
    }

    next();
  });
}

export default authMiddleware;
