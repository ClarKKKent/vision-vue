import Vue from 'vue';
import Vuex from 'vuex';

import auth from '@/auth/store/auth';
import exercises from '@/sport/store/exercises';
import users from '@/user/store/users';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    auth,
    users,
    exercises
  }
});
