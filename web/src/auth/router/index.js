const authRoutes = [
  {
    path: 'signin',
    name: 'auth-signin',
    component: () => import('@/auth/views/SigninView'),
    meta: {
      public: true,
      onlyWhenLoggedOut: true
    }
  }
];

export default authRoutes;
