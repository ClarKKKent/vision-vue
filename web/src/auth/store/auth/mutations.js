import * as types from './types';

const mutations = {
  [types.SIGNIN_REQUEST](state) {
    state.authenticating = true;
  },
  [types.SIGNIN_SUCCESS](state, token) {
    state.authenticating = false;
    state.token = token;
  },
  [types.SIGNIN_ERROR](state, error) {
    state.authenticating = false;
    state.authenticatedErrorMessage = error.response.data.detail;
    state.authenticatedErrorCode = error.response.status;
  },
  [types.LOGOUT_SUCCESS](state) {
    state.token = undefined;
  }
};

export default mutations;
