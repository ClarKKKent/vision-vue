import * as authProvider from '@/auth/utils/auth';

const state = () => ({
  authenticating: false,
  token: authProvider.getToken() ? authProvider.getToken() : undefined,
  authenticatedErrorMessage: '',
  authenticatedErrorStatus: 0
});

export default state;
