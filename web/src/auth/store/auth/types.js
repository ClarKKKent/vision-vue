export const SIGNIN_REQUEST = 'SIGNIN_REQUEST';

export const SIGNIN_SUCCESS = 'SIGNIN_SUCCESS';

export const SIGNIN_ERROR = 'SIGNIN_ERROR';

export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';

export const signinUser = 'auth/signin';

export const logoutUser = 'auth/logout';

export const isAuthenticating = 'auth/isAuthenticating';

export const isAuthenticatedError = 'auth/isAuthenticatedError';
