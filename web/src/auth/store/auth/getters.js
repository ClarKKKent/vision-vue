const getters = {
  isAuthenticating: (state) => {
    return state.authenticating;
  },
  isAuthenticatedError: (state) => {
    return !!state.authenticatedErrorMessage;
  }
};

export default getters;
