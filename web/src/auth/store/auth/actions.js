import signinUser from '@/auth/api/auth';
import * as authProvider from '@/auth/utils/auth';
import queryAsync from '@/core/api/query-async';
import router from '@/core/router';

import * as types from './types';

const actions = {
  async signin({ commit }, data) {
    commit(types.SIGNIN_REQUEST);
    const { response, error } = await queryAsync({
      fn: signinUser,
      params: data
    });
    if (error) {
      commit(types.SIGNIN_ERROR, error);
      return false;
    }
    const { token } = response.data;
    authProvider.login(token);
    commit(types.SIGNIN_SUCCESS, token);

    await router.push({ name: 'dashboard' });

    return true;
  },
  async logout({ commit }) {
    authProvider.logout();
    commit(types.LOGOUT_SUCCESS);

    await router.push({ name: 'auth-signin' });
  }
};

export default actions;
