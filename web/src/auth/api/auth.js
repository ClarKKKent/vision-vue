import api from '@/core/api';

const URLS = {
  signinUser: '/api/v1/auth/signin'
};

function signinUser(data) {
  return api(URLS.signinUser, { method: 'POST', data });
}

export default signinUser;
