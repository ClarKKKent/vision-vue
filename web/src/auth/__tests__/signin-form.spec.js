import { createLocalVue, mount } from '@vue/test-utils';
import Vue from 'vue';
import Vuetify from 'vuetify';
import Vuex from 'vuex';

import SigninForm from '@/auth/components/SigninForm';

const localVue = createLocalVue();

Vue.use(Vuetify);
localVue.use(Vuex);

describe('SigninForm.vue', () => {
  let wrapper;
  let store;
  let vuetify;

  const signin = jest.fn();

  const form = {
    email: 'test@test.com',
    password: 'password'
  };

  beforeEach(() => {
    store = new Vuex.Store({
      modules: {
        auth: {
          namespaced: true,
          getters: {
            isAuthenticating: () => false,
            isAuthenticatedError: () => false
          },
          actions: {
            signin
          }
        }
      }
    });

    vuetify = new Vuetify();
    wrapper = mount(SigninForm, {
      localVue,
      store,
      vuetify
    });
  });

  it('should sets initial value on input', async () => {
    await wrapper.setData({
      form
    });

    expect(wrapper.findAll('input').at(0).element.value).toEqual(
      'test@test.com'
    );
    expect(wrapper.findAll('input').at(1).element.value).toEqual('password');
  });

  it('should action be dispatched when the form is submitted', async () => {
    await wrapper
      .findAll('input')
      .at(0)
      .setValue('test@test.com');
    await wrapper
      .findAll('input')
      .at(1)
      .setValue('password');

    wrapper.find('form').trigger('submit');

    expect(signin).toBeCalled();
  });

  it('should action be dispatched with params equals form values', async () => {
    store.dispatch = jest.fn();

    await wrapper
      .findAll('input')
      .at(0)
      .setValue('test@test.com');
    await wrapper
      .findAll('input')
      .at(1)
      .setValue('password');

    wrapper.find('form').trigger('submit');

    expect(store.dispatch).toHaveBeenCalledWith('auth/signin', {
      email: 'test@test.com',
      password: 'password'
    });
  });

  it('should alert be visible when authenticatedError Vuex getter', () => {
    store.hotUpdate({
      modules: {
        auth: {
          namespaced: true,
          getters: {
            isAuthenticating: () => false,
            isAuthenticatedError: () => true
          }
        }
      }
    });

    const localWrapper = mount(SigninForm, {
      localVue,
      store,
      vuetify
    });

    expect(localWrapper.html()).toContain('Неверный логин и/или пароль.');
  });

  it('should button loader be visible when authenticating.', () => {
    store.hotUpdate({
      modules: {
        auth: {
          namespaced: true,
          getters: {
            isAuthenticating: () => true,
            isAuthenticatedError: () => false
          }
        }
      }
    });

    const localWrapper = mount(SigninForm, {
      localVue,
      store,
      vuetify
    });

    expect(localWrapper.find('.v-btn__loader').exists()).toBe(true);
  });
});
