import axios from 'axios';

const LOCAL_STORAGE_TOKEN_KEY = 'access_token';

function getToken() {
  return window.localStorage.getItem(LOCAL_STORAGE_TOKEN_KEY);
}

function setToken(token) {
  window.localStorage.setItem(LOCAL_STORAGE_TOKEN_KEY, token);
}

function removeToken() {
  window.localStorage.removeItem(LOCAL_STORAGE_TOKEN_KEY);
}

function hasToken() {
  return !!window.localStorage.getItem(LOCAL_STORAGE_TOKEN_KEY);
}

function login(token) {
  setToken(token);
  axios.defaults.headers.common.Authorization = `Bearer ${token}`;
}

function logout() {
  removeToken();
  axios.defaults.headers.common = {};
}

export { login, logout, getToken, removeToken, hasToken };
