const esModules = ['lodash-es'].join('|');

module.exports = {
  preset: '@vue/cli-plugin-unit-jest',
  transformIgnorePatterns: [`/node_modules/(?!${esModules})`]
};
